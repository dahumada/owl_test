Feature: Performing a CNN Search

    As a user on the CNN page
    I want to search for NFL and NFLFake news

    Background:

        Given I'm on the CNN page
    
    Scenario: Performing NFL search 
        When  I click the search button
        And I write "NFL" into the search bar
        Then I should see the search result list
        
    Scenario Outline: Performing a search with no results 
        When  I click the search button
        And I write <searchItem> into the search bar
        Then I should not see any results

        Examples:
        |searchItem|
        |"NFLFake"|