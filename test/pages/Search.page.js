class SearchPage {

    get searchInput()   { return $('#header-search-bar'); }
    get searchIcon()  { return $('.search-icon'); }
    get resultsList()   { return $('.cnn-search__results'); }
    get noresults()     { return $('.cnn-search__no-results')}
    
    open () {
        browser.url('http://cnn.com')       
        browser.pause(500);
    }
  
    enterText (item) {
      this.searchInput.setValue(item);
      this.searchInput.keys("\uE007");
    }
  
    search () {
      this.searchIcon.click();
    }
    isSearched () {
      return this.resultsList.isDisplayed();
    }
    isSearchednull () {
        return this.noresults.isDisplayed();
      }
  }

export const searchPage = new SearchPage();
