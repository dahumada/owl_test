
import { Given, When, Then } from 'cucumber';
import { searchPage } from '../pages/Search.page';

Given(/^I'm on the CNN page$/, () => {
    searchPage.open();
});

When(/^I write "([^"]*)" into the search bar$/, function(iex) {
    searchPage.enterText(iex);
  });

When(/^I click the search button$/, function() {
    searchPage.search();
  });

Then(/^I should see the search result list$/, function() {
    searchPage.isSearched();
  });

  Then(/^I should not see any results$/, function() {
    searchPage.isSearchednull();
  });

